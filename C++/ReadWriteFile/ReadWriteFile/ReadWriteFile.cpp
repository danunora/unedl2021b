#include <iostream>
#include <stdlib.h>
#include <fstream>
using namespace std;

void write() {
    ofstream archivo;

    archivo.open("C:/TEMP/archivo.txt", ios::app);

    if (archivo.fail()) {
        cout << "No se puede abrir el archivo";
        exit(1);
    }

    archivo << "Esta es una prueba\n";

    archivo.close();
}


int main()
{
    std::cout << "Hello World!\n";

    write();
    system("pause");

    return 0;
}

