#include <fstream>
#include <iostream>
#include <string>
using namespace std;

char filename[] = "archivo.txt";

void test1()
{
	char data[100];

	// open a file in write mode.
	ofstream outfile;
	outfile.open(filename);

	cout << "Writing to the file" << endl;
	cout << "Enter your name: ";
	cin.getline(data, 100);

	// write inputted data into the file.
	outfile << data << endl;

	cout << "Enter your age: ";
	cin >> data;
	cin.ignore();

	// again write inputted data into the file.
	outfile << data << endl;

	// close the opened file.
	outfile.close();
}

void test2()
{
	char data[100];

	// open a file in read mode.
	ifstream infile;

	infile.open(filename);

	cout << "Reading from the file" << endl;
	infile >> data;

	// write the data at the screen.
	cout << data << endl;

	// again read the data from the file and display it.
	infile >> data;

	cout << data << endl;

	// close the opened file.
	infile.close();
}

int test3() 
{
	cout << "Writing lines to the file" << endl;
	ofstream myfile(filename);
	if (myfile.is_open())
	{
		myfile << "This is a line" << endl;
		myfile << "This is another line" << endl;
		myfile.close();
	}
	else
	{
		cout << "Unable to open the file";
		return 1;
	}
	return 0;
}

int test4()
{
	cout << "Reading from the file" << endl;
	string line;
	ifstream myfile(filename);
	if (myfile.is_open())
	{
		while (getline(myfile, line)) {
			cout << line << endl;
		}
		myfile.close();
	}
	else
	{
		cout << "Unable to open the file";
		return 1;
	}
	return 0;
}

int test5() 
{
	cout << "Obtaining the file size" << endl;
	streampos begin, end;
	ifstream myfile(filename, ios::binary);
	begin = myfile.tellg();
	myfile.seekg(0, ios::end);
	end = myfile.tellg();
	myfile.close();
	cout << "size is: " << (end - begin) << "bytes." << endl;
	return 0;
}

int test6()
{
	cout << "Loading file into memory" << endl;
	streampos size;
	char * memblock;
	ifstream myfile(filename, ios::in|ios::binary|ios::ate);
	if (myfile.is_open())
	{
		size = myfile.tellg();
		memblock = new char[size];
		myfile.seekg(0, ios::beg);
		myfile.read(memblock, size);
		myfile.close();
		cout << "the entire file content is in memory";
		delete[] memblock;
	}
	else
	{
		cout << "Unable to open the file";
		return 1;
	}
	return 0;
}

int main() {

	test1();

	test2();

	if (test3() != 0) {
		cout << "Error test3" << endl;
	}

	if (test4() != 0) {
		cout << "Error test4" << endl;
	}

	if (test5() != 0) {
		cout << "Error test5" << endl;
	}

	return 0;
}