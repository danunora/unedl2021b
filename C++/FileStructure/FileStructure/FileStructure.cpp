#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;

struct Direccion {
	char calle[20];
	char numero[5];
	char interior[5];
} direccion;

struct Personas {
	int id;
	char nombre[20];
	char apellido[20];
	Direccion direccion;
//	char direccion[50];
//	char telefono[20];
//	char email[20];
} persona[2];


int main()
{
	// open a file in write mode.
	ofstream outfile;
	outfile.open("personas.dat");

	for (int i = 0; i < 2; i++) {
		fflush(stdin);

		cout << "Digite el id: ";
		cin >> persona[i].id;

		cin.ignore(numeric_limits<streamsize>::max(), '\n');

		cout << "Nombre: ";
		cin.getline(persona[i].nombre, 20, '\n');

		cout << "Apellido: ";
		cin.getline(persona[i].apellido, 20, '\n');

		cout << "Calle: ";
		cin.getline(persona[i].direccion.calle, 20, '\n');

		cout << endl;
	}

	for (int i = 0; i < 2; i++) {
		cout << "      id: " << persona[i].id << endl;
		outfile << persona[i].id << endl;
		cout << "  Nombre: " << persona[i].nombre << endl;
		outfile << persona[i].nombre << endl;
		cout << "Apellido: " << persona[i].apellido << endl;
		outfile << persona[i].apellido << endl;
		cout << "----------" << endl;
		cout << "    Calle:" << persona[i].direccion.calle << endl;
		outfile << persona[i].direccion.calle << endl;
	}
	outfile.close();
}

