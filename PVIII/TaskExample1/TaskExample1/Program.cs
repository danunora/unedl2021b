﻿using System;
using System.Threading;
using System.Threading.Tasks;

public class Example
{
    public static void Main()
    {
        Thread.CurrentThread.Name = "Main";

        // Create a task and supply a user delegate by using a 
        // lambda expression.

        Task taskA = 
            new Task(() => Console.WriteLine("Hello from taskA."));

        // Define and run the task.
        Task taskB = 
            Task.Run(() => Console.WriteLine("Hello from taskB."));

        // Start the task.
        taskA.Start();

        // Output a message from the calling thread.
        Console.WriteLine("Hello from thread '{0}'.",
                          Thread.CurrentThread.Name);
        taskA.Wait();
        taskB.Wait();
    }
}
// The example displays output like the following:
//       Hello from thread 'Main'.
//       Hello from taskA.