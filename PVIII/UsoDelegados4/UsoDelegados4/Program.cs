using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsoDelegados4
{
    class Program
    {
        delegate void midelegado();

        delegate void apuntador();

        static void Despliega()
        {
            var cadena = 1;
            var resultado = 100 / cadena;
            Console.WriteLine("metodo desplegar {0}",resultado);
        }

        static void Main(string[] args)
        {
            midelegado delegado = new midelegado(Despliega);

            // metodo anonimo
            apuntador apunta = delegate
            {
                Console.WriteLine("este es un metodo anonimo");
            };

            delegado();

            apunta();

            Console.ReadKey();

        }
    }
}
