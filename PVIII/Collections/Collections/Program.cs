using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    class Program
    {
        static void myQueue()
        {
            Queue q = new Queue();

            q.Enqueue('A');
            q.Enqueue('M');
            q.Enqueue('G');
            q.Enqueue('W');
            q.Enqueue("String");
            q.Enqueue(23);
            q.Enqueue(232.3);

            Console.WriteLine("Current queue: ");
            foreach (var c in q) Console.WriteLine(c + " ");

            Console.ReadKey();

            //Console.WriteLine();
            //q.Enqueue('V');
            //q.Enqueue('H');
            //Console.WriteLine("Current queue: ");
            //foreach (char c in q) Console.Write(c + " ");

            //Console.WriteLine();
            //Console.WriteLine("Removing some values ");
            //char ch = (char)q.Dequeue();
            //Console.WriteLine("The removed value: {0}", ch);
            //ch = (char)q.Dequeue();
            //Console.WriteLine("The removed value: {0}", ch);
        }


        static void myStack()
        {
            Stack mystack = new Stack();

            mystack.Push("Daniel");
            mystack.Push("Alejandro");
            mystack.Push(7);
            mystack.Push(18.32);
            mystack.Push('u');

            Console.WriteLine("Elemento 7: {0}", mystack.Contains(7));

            Console.WriteLine("Peek : {0}", mystack.Peek());

            while(mystack.Count != 0)
            {
                Console.WriteLine("Sacando el elemento: {0}", 
                    mystack.Pop());
            }
        }


        static void mySortedList()
        {
            SortedList myarray = new SortedList();

            SortedList otroarray = new SortedList();
            myarray.Add(1, "Daniel");
            myarray.Add(3, "Alejandro");
            myarray.Add(4, "Nuno");
            myarray.Add(2, "Ramirez");
            myarray.Add(5, 35);
            myarray.Add(6, 10.33);
            myarray.Add(7, 'y');
            // myarray.Add(3, "Error");

            otroarray.Add("a", "Rodrigo");
            otroarray.Add("b", "Hernandez");
            otroarray.Add("d", "Juarez");
            otroarray.Add("z", "Zopilota");
            //otroarray.Add("a", "error");
            IEnumerable llaves = myarray.Keys;

            IEnumerable llaves2 = otroarray.Keys;

            foreach(var i in llaves)
            {
                Console.WriteLine("Llave: {0}",i);
                Console.WriteLine("Valor: {0}",myarray[i]);
            }

            Console.WriteLine("----------------");

            foreach (var j in llaves2)
            {
                Console.WriteLine("Llave: {0}",j);
                Console.WriteLine("Valor: {0}",otroarray[j]);
            }
        }

        static void myArrayList()
        {
            ArrayList myarray = new ArrayList();

            myarray.Add("Daniel");
            myarray.Add(9);
            myarray.Add(10.232);
            myarray.Add(7);
            myarray.Add(1.3);
            myarray.Add("Alejandro");
            myarray.Add("DANIEL");
            myarray.Add("Daniel");
            myarray.Add('c');

            // ordena elementos del mismo tipo
            //myarray.Sort();

            //myarray.Clear();

            foreach (var i in myarray)
            {
                Console.WriteLine("Elemento: {0}", i);
            }

            Console.WriteLine("#######################");
            if (myarray.Contains("DANIEL"))
            {
                Console.WriteLine("Elemento encontrado");
            }
            else
            {
                Console.WriteLine("No fue encontrado");
            }

        }

        static void Main(string[] args)
        {
            // ArrayList
            //myArrayList();

            // SortedList
            //mySortedList();

            // Pila LIFO
            // myStack();

            // Queue FIFO
            // myQueue();

        }
    }
}
