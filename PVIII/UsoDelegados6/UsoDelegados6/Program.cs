using System;

namespace Delegates
{
    // Delegado
    public delegate void DelEventHandler();

    class Program
    {
        // Evento
        public static event DelEventHandler add;

        static void Main(string[] args)
        {
            add += new DelEventHandler(USA);
            add += new DelEventHandler(India);
            add += new DelEventHandler(England);

            // Invocando al evento
            add.Invoke();

            Console.ReadLine();
        }

        static void USA()
        {
            Console.WriteLine("USA");
        }

        static void India()
        {
            Console.WriteLine("India");
        }

        static void England()
        {
            Console.WriteLine("England");
        }
    }
}