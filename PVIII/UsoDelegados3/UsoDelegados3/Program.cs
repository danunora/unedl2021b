using System;

namespace Delegates
{
    // Delegate Definition  
    public delegate int operation(int x, int y);

    public delegate void operacion2(String c);

    public class ClaseDelegadosPrueba
    {
        public void miOtroMetodo(String cadena)
        {
            Console.WriteLine("Imprimir cadena: {0}", cadena);
        }
    }

    class Program
    {
        // Method that is passes as an Argument  
        // It has same signature as Delegates   
        static int Addition(int a, int b)
        {
            return a + b;
        }

        static int MiMetodo(int a, int b)
        {
            if((a - b >= 0))
                Console.WriteLine("MiMetodo: {0} y luego {1}",0,0);
            else
                Console.WriteLine("MiMetodo: {0} y luego {1}",1,1);
            Console.WriteLine("termina mi metodo");
            return 0;
        }

        static void Main(string[] args)
        {
            // Delegate instantiation  
            operation obj = new operation(Program.Addition);

            operation miMetodo = new operation(Program.MiMetodo);

            ClaseDelegadosPrueba test = new ClaseDelegadosPrueba();

            operacion2 miOtroMetodo = 
                new operacion2(test.miOtroMetodo);

            // output  
            Console.WriteLine("Addition is={0}", obj(23, 27));

            Console.WriteLine("Ejecutando miMetodo");
            miMetodo(100,80);

            Console.WriteLine("Ejecutando metodo de otra clase");

            test.miOtroMetodo("Alejandro");

            miOtroMetodo("Daniel");

            Console.ReadLine();
        }
    }
}