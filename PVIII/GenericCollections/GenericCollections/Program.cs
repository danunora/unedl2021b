using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericCollections
{
    class Program
    {
        static void ListExample()
        {
            Personas persona1 = new Personas();
            persona1.Nombre = "Daniel";
            persona1.Apellido = "Nuno";
            persona1.Edad = 49;

            Personas persona2 = new Personas("Alejandro", "Ramirez", 50);

            List<Personas> mypersonas = new List<Personas>();

            mypersonas.Add(persona1);
            mypersonas.Add(persona2);

            // Quick Sort
            mypersonas.Sort();

            Console.WriteLine("Cantidad de personas: {0}",
                mypersonas.Count());

            Servidores server1 = new Servidores();

            List<Servidores> servidores = new List<Servidores>();

            servidores.Add(server1);
        }

        static void HashExample()
        {
            HashSet<Personas> personas = new HashSet<Personas>();

            Personas persona1 = new Personas();
            persona1.Nombre = "Daniel";
            persona1.Apellido = "Nuno";
            persona1.Edad = 49;

            Personas persona2 = new Personas("Alejandro", "Ramirez", 50);

            personas.Add(persona1);
            personas.Add(persona2);

            foreach (var persona in personas)
            {
                Console.WriteLine("Persona: ", persona.Nombre);
            }

        }

        static void Main(string[] args)
        {

            HashExample();
            
        }
    }
}
