﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericCollections
{
    class Servidores
    {
        String nombre;
        String osversion;
        int cpuinfo;
        int raminfo;
        int hddinfo;
        String posicion;
        String owner;
        String ip_adress;

        public Servidores()
        {
        }

        public Servidores(string nombre, string osversion, int cpuinfo, 
            int raminfo, int hddinfo, string posicion, string owner, 
            string ip_adress)
        {
            this.nombre = nombre;
            this.osversion = osversion;
            this.cpuinfo = cpuinfo;
            this.raminfo = raminfo;
            this.hddinfo = hddinfo;
            this.posicion = posicion;
            this.owner = owner;
            this.ip_adress = ip_adress;
        }

        public string Nombre { get => nombre; set => nombre = value; }
        public string Osversion { get => osversion; set => osversion = value; }
        public int Cpuinfo { get => cpuinfo; set => cpuinfo = value; }
        public int Raminfo { get => raminfo; set => raminfo = value; }
        public int Hddinfo { get => hddinfo; set => hddinfo = value; }
        public string Posicion { get => posicion; set => posicion = value; }
        public string Owner { get => owner; set => owner = value; }
        public string Ip_adress { get => ip_adress; set => ip_adress = value; }
    }
}
