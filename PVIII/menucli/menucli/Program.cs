﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menucli
{
    class Program
    {
        /*
         * Menu Principal
         */
        static bool MenuPrincipal()
        {
            String opcion = "";
            bool respuesta = true;
            Console.Clear();
            Console.WriteLine("Seleccione la opción:");
            Console.WriteLine("1) Recorrido matriz");
            Console.WriteLine("2) Recorrido matriz");
            Console.WriteLine("3) Recorrido matriz");
            Console.WriteLine("0) Salir");
            opcion = Console.ReadLine().Trim();
            switch (opcion)
            {
                case "1" :
                    break;
                case "2" :
                    break;
                case "3" :
                    break;
                case "0":
                    respuesta = false;
                    break;
                default:
                    Console.WriteLine("opción inválida");
                    Console.ReadKey();
                    break;
            } 
            return respuesta;
        }

        /*
         * Main Method
         */
        static void Main(string[] args)
        {
            bool continuar = true;

            while (continuar)
            {
                continuar = MenuPrincipal();
            }
        }

    }
}
