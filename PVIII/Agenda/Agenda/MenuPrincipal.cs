using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace Agenda
{
    public partial class MenuPrincipal : Form
    {
        public MenuPrincipal()
        {
            InitializeComponent();
        }

        private void MenuPrincipal_Load(object sender, EventArgs e)
        {
            this.Text = "Agenda de la UNEDL";
        }

        private void recordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // ventana de registros
            Registro registro = new Registro();
            registro.MdiParent = this;
            registro.Show();
        }

        private void existToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void manualToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Manual manual = new Manual();
            manual.MdiParent = this;
            manual.Show();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox about = new AboutBox();
            about.MdiParent = this;
            about.Show();
        }
    }
}
