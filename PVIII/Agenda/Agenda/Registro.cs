﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Agenda
{
    /*
     * ToDO:
     * 1) Validar telefono unicamente numerico
     * 2) Meter el registro en formato separado por comas (csv)
     * 3) Obtener del archivo de registro el último ID usado, para generar el nuevo
     * 4) Realizar una clase separada para manejar los mensajes de error (MessageBox)
     * 5) Agregar una opcion para generar el archivo de la agenda
     */ 

    public partial class Registro : Form
    {
        public Registro()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            // Metodo para registrar persona

            // Validar Campos
            if (!validaRegistros())
            {
                // Initializes the variables to pass to the MessageBox.Show method.
                string message = "Valores inválidos";
                string caption = "Error Detectado a la entrada";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;

                // Displays the MessageBox.
                result = MessageBox.Show(message, caption, buttons);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    inicializaRegistro();
                
                    // Closes the parent form.
                    //this.Close();
                }
            }
            else
            {
                // Registro Persona
                if (!registroPersona())
                {
                    // Initializes the variables to pass to the MessageBox.Show method.
                    string message = "Hubo un error al guardar el registro";
                    string caption = "Error detectado al guardar el registro";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult result;

                    // Displays the MessageBox.
                    result = MessageBox.Show(message, caption, buttons);
                    if (result == System.Windows.Forms.DialogResult.Yes)
                    {
                        inicializaRegistro();

                        // Closes the parent form.
                        //this.Close();
                    }
                }
            }
            inicializaRegistro();
        }

        /*
         * Inicializa valores
         */ 
        private void inicializaRegistro()
        {
            txtID.Value = 1;
            txtNombre.Text = "";
            txtTelefono.Text = "";
        }

        /*
         * Validar Registros
         * regresa: 
         * false, si hay registros invalidos
         * true, si todos los registros estan correctos
         */
        private bool validaRegistros()
        {
            bool resultado = true;
            if(txtID.Value < 1)
            {
                resultado = false;
            }
            if(txtNombre.Text.Length < 1 )
            {
                resultado = false;
            }
            if(txtTelefono.Text.Length < 1)
            {
                resultado = false;
            }
            return resultado;
        }

        private void Registro_Load(object sender, EventArgs e)
        {
            inicializaRegistro();
        }

        /*
         * 
         * 
         */
        private bool registroPersona()
        {
            bool resultado = true;

            // Create a string array with the lines of text
            string[] lines = 
                { txtID.Value.ToString(), txtNombre.Text, txtTelefono.Text};

            // Set a variable to the Documents path.
            string docPath =
              Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            try
            {
                // Write the string array to a new file named "WriteLines.txt".
                using (StreamWriter outputFile =
                    new StreamWriter(Path.Combine(docPath, "Registro.txt")))
                {
                    foreach (string line in lines)
                        outputFile.WriteLine(line);
                }
            }
            catch (Exception e)
            {
                resultado = false;
            }
            return resultado;
        }


    }
}
