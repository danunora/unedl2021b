#include <iostream>
#include <stdlib.h>
#include <fstream>

using namespace std;

ofstream archivo;

void abrir_archivo() 
{
	archivo.open("C:/Temp/archivo.txt");

	if (archivo.fail()) {
		cout << "Unable to open the file !";
		exit(1);
	}
	
}

int main()
{
    std::cout << "Hello World!\n";

	abrir_archivo();
	archivo << "registro" << endl;

	system("pause");
	return 0;
}
