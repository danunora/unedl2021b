#include <fstream>
#include <iostream>
#include <stdlib.h>

using namespace std;

void gestionarchivo() 
{
	ofstream archivo;
	char data[100];

	cout << "######### Abriendo el archivo" << endl;

	archivo.open("C:/Temp/miarchivo.txt",ios::app);

	if (archivo.fail()) {
		std::cout << "No pudo abrir el archivo!";
	}

	cout << "Dame tu nombre: " << endl;
	cin.getline(data, 100);

	cout << "######### Escribiendo al archivo" << endl;

	archivo << data << endl;

	cout << "######### Cerrando el archivo" << endl;

}


int main()
{
	gestionarchivo();

    std::cout << "Hello World!\n";

	system("pause");
}

