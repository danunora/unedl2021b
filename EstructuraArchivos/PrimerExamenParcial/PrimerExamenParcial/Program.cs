using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PrimerExamenParcial
{
    class Program
    {
        static string Normalize(string word)
        {
            var arrayText = word.ToLower().ToCharArray();
            var newword = "";
            foreach (char letter in arrayText)
            {
                switch (letter)
                {
                    case ' ':
                        newword += "";
                        break;
                    case 'á':
                        newword += "a";
                        break;
                    case 'é':
                        newword += "e";
                        break;
                    case 'í':
                        newword += "i";
                        break;
                    case 'ó':
                        newword += "o";
                        break;
                    case 'ú':
                        newword += "u";
                        break;
                    case 'ñ':
                        newword += "n";
                        break;
                    default:
                        newword += letter;
                        break;
                }
            }
            return newword;
        }

        // Un palíndromo es una palabra o frase que se lee igual 
        // de izquierda a derecha que de derecha a izquierda.
        static bool isPalindromo(string word)
        {
            bool resultado = true;
            var newword = Normalize(word);
            //Console.WriteLine("newword: " + newword);
            var revword = new string(newword.ToCharArray().Reverse().ToArray());
            //Console.WriteLine("revword: " + revword);
            if (!newword.Equals(revword))
                resultado = false;
            return resultado;
        }

        static bool IsPalindrome(string str)
        {
            str = new Regex("[^a-zA-Z]").Replace(str, "").ToLower();
            Console.WriteLine("str: {0}",str);
            return !str.Where((t, i) => t != str[str.Length - i - 1]).Any();
        }

        static string RemoveDiacritics(string input)
        {
            string normalized = input.Normalize(NormalizationForm.FormD);
            var builder = new StringBuilder();

            foreach (char ch in normalized)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(ch) != UnicodeCategory.NonSpacingMark)
                {
                    builder.Append(ch);
                }
            }

            return builder.ToString().Normalize(NormalizationForm.FormC);
        }

        // Metodo principal
        static void Main(string[] args)
        {
            string[] sarray = { "Acaso hubo búhos acá", "A cavar a Caravaca",
                "Allí ves Sevilla", "Amad a la dama", "no es fiesta", "redivider"};
            string newword;
            foreach (string word in sarray)
            {
                newword = RemoveDiacritics(word);             
                if(IsPalindrome(newword))
             //   if (isPalindromo(word))
                   Console.WriteLine("La frase {0} es un palíndromo", word);
                else
                   Console.WriteLine("La frase {0} no es un palíndromo",word);
            }
        }
    }
}
