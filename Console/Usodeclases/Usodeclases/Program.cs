﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usodeclases
{
    class Program
    {
        // Structure
        struct Books
        {
            public string title;
            public string author;
            public string subject;
            public int book_id;
        };

        /*
        *  Display a given array
        */
        static void DisplayArray(int[][] multiDimensionalArr)
        {
            for (int i = 0; i < multiDimensionalArr.Length; i++)
            {
                Console.Write("Element({0}): ", i);
                for (int j = 0; j < multiDimensionalArr[i].Length; j++)
                {
                    Console.Write("{0}{1}", multiDimensionalArr[i][j],
                        j == (multiDimensionalArr[i].Length - 1) ? "" : " ");
                }
                Console.WriteLine();
            }
        }

        static void Main(string[] args)
        {
            Persona persona1 = new Persona();
            Persona persona2 
                = new Persona("Alejandro", "Ramirez", 48, 1.73);
            Persona persona3 = 
                new Persona("Belen", "Martinez", 23, 1.70,50,true);

            persona1.Nombre = "Daniel";
            persona1.Apellido = "Nuno";
            persona1.Edad = 48;
            persona1.Estatura = 1.72;
            persona1.Sexo = false;  // false = masculino
            persona1.setPeso(80);

            Console.WriteLine("Nombre: " + persona1.Nombre);
            Console.WriteLine("Apellido: " + persona1.Apellido);
            Console.WriteLine("Peso: " + persona1.getPeso());

            Console.WriteLine("Accion persona1:");
            persona1.Caminar();
            Console.WriteLine("Accion persona2:");
            persona2.Caminar();
            Console.WriteLine("Accion persona3:");
            persona3.Caminar();

            Console.WriteLine("#############################");

            Soldado soldado1 = new Soldado();

            Console.WriteLine("Accion soldado1:");
            soldado1.Caminar();
            soldado1.Disparar();
            soldado1.Nadar();
            soldado1.DispararFusil(6);
            soldado1.UsarRadar();

            Books book1;
            book1.title = "Solaris";
            book1.author = "Bill Calklins";
            book1.subject = "sistema operativo Solaris";
            book1.book_id = 100;

            Console.WriteLine("book1 title: " + book1.title);
            Console.WriteLine("book1 author: " + book1.author);

            int[] arreglo1 = new int[5];
            arreglo1[0] = 21;
            arreglo1[1] = 30;
            arreglo1[2] = 89;
            arreglo1[3] = 0;
            arreglo1[4] = 76;

            int[,] arreglo2 = new int[2, 5];
            arreglo2[0, 1] = 23;
            arreglo2[1, 2] = 45;

            //int[][] arrayofarrays = new int[2][2];

            int[][] multiDimensionalArr = new int[3][];
            multiDimensionalArr[0] = new int[] { 1, 2 };
            multiDimensionalArr[1] = new int[] { 3, 4, 5 };
            multiDimensionalArr[2] = new int[] { 6, 7, 8, 9 };

            DisplayArray(multiDimensionalArr);

            Console.ReadKey();
        }
    }
}
