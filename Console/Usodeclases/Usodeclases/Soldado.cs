﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usodeclases
{
    class Soldado : Persona, IArmamento
    {
        private bool pistola;
        private bool casco;

        public Soldado()
        {
            Console.WriteLine("Constructor Soldado");
            this.pistola = false;
            this.casco = false;
        }

        public Soldado(bool pistola, bool casco)
        {
            Console.WriteLine("Constructor Soldado dos parametros");
            this.pistola = pistola;
            this.casco = casco;
        }

        public new void Caminar()
        {
            Console.WriteLine("Soldado Marchando");
        }

        public void Disparar()
        {
            Console.WriteLine("Soldado dispara");
        }

        public void DispararFusil(int balas)
        {
            for (int i = 1; i <= balas; i++)
            {
                Console.Write("disparando bala: " +i);
            }
            Console.WriteLine("Se acabaron las balas");
        }

        public int UsarRadar()
        {
            Console.WriteLine("Usando Radar");
            return 0;
        }
    }
}
