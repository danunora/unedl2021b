﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usodeclases
{
    class Persona
    {
        private String nombre;
        private String apellido;
        private int edad;
        private double estatura;
        private bool sexo;
        private double peso;

        // Especifica de C#
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public int Edad { get => edad; set => edad = value; }
        public double Estatura { get => estatura; set => estatura = value; }
        public bool Sexo { get => sexo; set => sexo = value; }

        // similar a Java
        public void setPeso(double peso)
        {
            this.peso = peso;
        }

        public double getPeso()
        {
            return peso;
        }

        // Constructores

        // Constructor vacio
        public Persona()
        {
            Console.WriteLine("constructor persona vacio");
            this.nombre = "";
            this.apellido = "";
            this.edad = 0;
            this.estatura = 0;
            this.peso = 0;
        }

        // constructor 6 campos
        public Persona(string nombre, string apellido, 
            int edad, double estatura,  double peso, bool sexo)
        {
            Console.WriteLine("constructor persona seis campos");
            this.nombre = nombre;
            this.apellido = apellido;
            this.edad = edad;
            this.estatura = estatura;
            this.sexo = sexo;
            this.peso = peso;
        }

        // constructor 4 campos
        public Persona(string nombre, string apellido, 
            int edad, double estatura)
        {
            Console.WriteLine("constructor persona cuatro campos");
            this.nombre = nombre;
            this.apellido = apellido;
            this.edad = edad;
            this.estatura = estatura;
        }

        public void Caminar()
        {
            Console.WriteLine("Persona caminando");
        }

        public void Nadar()
        {
            Console.WriteLine("Persona nadando");
        }


    }
}
